import BlockKhachHang from '../componensGroup/BlockKhachHang'
import BlockTinTuc from '../componensGroup/BlockTinTuc'
import BlockUtilities from '../componensGroup/BlockUtilities'
import BlockWhy from '../componensGroup/BlockWhy'
import CategoryProtect from '../componensGroup/bloc_protect_prod'
import DowloadApp from '../componensGroup/DowloadApp'
import Footer from '../componensGroup/Footer'
import ProductsInterest from '../componensGroup/ProductsInterest'
import Qna from '../componensGroup/qna'
import MainLayout from '../MainLayout'

export default function Home() {
  return (
    <MainLayout isHome={true}>
      <ProductsInterest/>
      <CategoryProtect/>
      <BlockUtilities/>
      <BlockTinTuc/>
      <BlockWhy/>
      <BlockKhachHang/>
      <DowloadApp/>
      <Qna/>
      <Footer/>
    </MainLayout>
  )
}
