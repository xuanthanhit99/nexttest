import React from 'react'
import data from "../json/BlockKh.json"
import { BlockKhIg, ListKh, Title } from '../src/vbi_uikit'

const BlockKhachHang = () => {
  const dataKh = data?.define?.title
  const dataKhImage = data?.define?.properties?._ite?.banner_slide
  const dataListKh = data?.define?.properties?._ite?.list_kh
  return (
    <div>
         <div className="uk-section block_danhgiakhachhangdoitac__section">
      <div className="item__60">
        <div className="uk-container">
          <Title title={dataKh}/>
          <BlockKhIg dataKhImage={dataKhImage}/>
        </div>
      </div>
      <ListKh dataListKh={dataListKh}/>
    </div>
    </div>
  )
}

export default BlockKhachHang