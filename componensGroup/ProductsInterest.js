import React from 'react'
import data from "../json/ProductsInterest.json"
import { BannerSlice, Slide, Title } from '../src/vbi_uikit';

const ProductsInterest = () => {
  const SlideData = data?.define?.properties?._ite?.list_href
  const bannerData = data?.define?.properties?._ite?.banner_slide
  return (
    <div>
      <div className="home__block01__bg uk-background-image@m uk-flex uk-flex-middle">
        <div className="uk-width-1-1 uk-section">
          <div className="uk-container uk-container-small">
            <Title title={data?.define?.title}/>
            <Slide SlideData={SlideData} />
            <BannerSlice bannerData={bannerData}/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductsInterest