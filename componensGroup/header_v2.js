
import data from "../json/header.json"
import { Navbar, NavbarTop, Scroll } from "../src/vbi_uikit";
// import Custom404 from "../../pages/404";

const Main = ({isHome}) => {
  if(data?.define === null) return <Custom404/>
  const dataNavbar = data?.define?.properties?._ite
  return (
    <div className="not_home" >
      {isHome? <NavbarTop dataNavbar={dataNavbar}/> : ""}
      <div className="header__dropdown" data-uk-sticky="">
        <div className="uk-container uk-padding-remove">
          <Navbar dataNavbar={dataNavbar}/>
          <Scroll dataNavbar={dataNavbar}/>
        </div>
      </div>
    </div>
  );
};


export default Main;

