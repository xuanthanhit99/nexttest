import React from 'react'
import data from '../json/DataTinTuc.json'
import { TinTucList, Title } from '../src/vbi_uikit'

const BlockTinTuc = () => {
    const dataTintuc = data?.define?.properties?._ite
    return (
        <>
            <div className="uk-section home__blockNews__section">
                <div className="uk-container">
                    {dataTintuc.map((itemTintuc) => {
                       return <TinTucList itemTintuc={itemTintuc} key={itemTintuc.id} />
                    })}
                </div>
            </div>
        </>
    )
}

export default BlockTinTuc