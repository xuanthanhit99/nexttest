import React from 'react'
import data from "../json/WhyData.json"
import { Title, WhyList } from '../src/vbi_uikit'

const BlockWhy = () => {
    const WhyDatas = data?.define?.properties?._ite
    return (
        <div className="home__blockVisao__section uk-section uk-background-muted">
            <div className="uk-container">
                <Title title={data?.define?.title}/>
                <WhyList WhyDatas={WhyDatas}/>
            </div>
        </div>
    )
}

export default BlockWhy