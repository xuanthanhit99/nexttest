import React from 'react'
import { FooterHidden, FooterVisible } from '../src/vbi_uikit'
import data from '../json/FooterData.json'

const Footer = () => {
  const dataFooter = data?.define?.properties?._ite?.visible
  return (
    <div className="footer uk-section uk-light">
      <div className="uk-container">
        <FooterVisible dataFooter={dataFooter}/>
        {/* <FooterHidden/> */}
      </div>
    </div>
  )
}

export default Footer