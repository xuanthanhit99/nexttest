import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { FLSelect, QnaSec, Title } from '../src/vbi_uikit';
import datas from '../json/qnaData.json'

const Qna = () => {
    const titleData = datas?.define?.title
    const isHideSeeMore = datas?.define?.properties?._ite?.isHideSeeMore
    const isHideCategory = datas?.define?.properties?._ite?.isHideCategory
    const data = datas?.define?.properties?._ite?.questions_home

    const [idQuestionActive, setIdQuestionActive] = useState('');
    const [arrQuestion, setArrQuestion] = useState([]);
    const [dataQuestion, setDataQuestion] = useState([]);

    useEffect(() => {
        if (data) {
            setDataQuestion(data);
            setArrQuestion(data[0].content);
            setIdQuestionActive(data[0].id);
        }
    }, [data]);

    const handleChooseQuestion = (e, id) => {
        e.preventDefault();
        e.stopPropagation();
        let questions = dataQuestion.find((item) => item.id === id);
        setArrQuestion(questions.content || []);
        setIdQuestionActive(id);
    };

    const onChangeSelect = (e) => {
        let id = e.value;
        let questions = dataQuestion.find((item) => item.id === id);
        setArrQuestion(questions.content || []);
        setIdQuestionActive(id);
    }

    return (
        <div className='uk-section block_cauhoithuonggap__section'>
            <div className='uk-container'>
                <Title title={titleData} />
                <QnaSec onChangeSelect={onChangeSelect} handleChooseQuestion={handleChooseQuestion} dataQuestion={dataQuestion} arrQuestion={arrQuestion} isHideSeeMore={isHideSeeMore} isHideCategory={isHideCategory} idQuestionActive={idQuestionActive}/>
            </div>
        </div>
    );
};

export default Qna;
