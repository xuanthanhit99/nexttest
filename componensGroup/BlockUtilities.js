import React from 'react'
import data from "../json/BlockUtilities.json"
import { CategoryPr, Title } from '../src/vbi_uikit'

const BlockUtilities = () => {
    const dataUtilities = data?.define?.properties?._ite
    const dataTitle = data?.define?.title
  return (
        <div className="uk-section home__blockTienich__section uk-background-muted">
      <div className="uk-container uk-padding-remove">
        <Title title={dataTitle}/>
        <CategoryPr dataCategory={dataUtilities}/>
      </div>
    </div>
  )
}

export default BlockUtilities