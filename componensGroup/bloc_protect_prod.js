import data from "../json/CategoryProtect.json"
import { CategoryPr, Title } from "../src/vbi_uikit";

const CategoryProtect = () => {
  const dataTitle = data?.define?.title
  const dataCategory = data?.define?.properties?._ite
  return (
    <div className="uk-section home__block02__section" style={{marginBottom: 5}}>
      <div className="uk-container uk-padding-remove">
        <Title title={dataTitle}/>
        {dataCategory?.arrBaoVeBanVaGiaDinh.map((dataCategoryItem)=>{
          return <CategoryPr dataCategory={dataCategoryItem} key={dataCategoryItem.id}/>
        })}
      </div>
    </div>
  );
};

export default CategoryProtect;
