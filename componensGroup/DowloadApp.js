import React from 'react'
import data from '../json/DowLoad.json'
import { DowLd, Title } from '../src/vbi_uikit'

const DowloadApp = () => {
    const TitleData = data?.define?.title
    const dataList = data?.define?.properties?._ite
    return (
        <>
            <div className="block_taiapp__gradient uk-light uk-overflow-hidden">
                <div
                    className="block_taiapp__section uk-section uk-background-norepeat uk-background-cover"
                    data-src={TitleData.data_src}
                    data-uk-img=""
                >
                    <div className="uk-container">
                        <Title title={TitleData} />
                        <DowLd dataList={dataList} />
                    </div>
                </div>
            </div>
        </>
    )
}

export default DowloadApp