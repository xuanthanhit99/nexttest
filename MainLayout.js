import Header_v2 from "./componensGroup/header_v2";
import Head from "next/head";

const MainLayout = (props) => {
    const {isHome} = props;
    const metaObject = props.metaObject || {
        title: "Bảo hiểm VietinBank (VBI) | Tiên phong công nghệ",
        description:
            "Bảo hiểm VietinBank (VBI) là một trong những đơn vị tiên phong áp dụng công nghệ, cải tiến quy trình bồi thường với ứng dụng bảo hiểm số hàng đầu My VBI.",
    };
    return (
        <div
            id="app-vbi"
            className="uk-height-viewport uk-offcanvas-content uk-overflow-hidden uk-position-relative"
        >
            <Head>
                <title>{metaObject?.title || "VBI"}</title>
                <meta name="description" content={metaObject?.description || "VBI"}/>
                <meta name="google-site-verification" content="oX1tUQ7BC9MyNEU-qvOXstAetwX8DAtd_Y7MANMfJzI" />
                <link rel="icon" href="/images/Logo_m.png"/>
                <meta
                    name="keywords"
                    content="my vbi, vbi, vbi bảo hiểm, bảo hiểm vbi, vbi care, bảo hiểm vbi care, bảo hiểm sức khỏe vbi, bảo hiểm vietinbank, bảo hiểm sức khỏe vietinbank, công ty bảo hiểm vietinbank, bảo hiểm vbi vietinbank, vbi vietinbank, bảo hiểm sức khỏe, bảo hiểm ung thư, bảo hiểm vật chất xe ô tô, bảo hiểm tnds ô tô, bảo hiểm du lịch quốc tế, bảo hiểm tai nạn con người, bảo hiểm du lịch trong nước, bảo hiểm tnds xe máy, bảo hiểm du lịch, bảo hiểm tnds, bảo hiểm sức khỏe cho bé, bảo hiểm"
                />

                <link rel="canonical" href="http://myvbi.vn/ "/>
                <link rel="alternate" hrefLang="vi" href="http://myvbi.vn/ "/>
                <meta property="og:locale" content="vi_VN"/>
                <meta property="og:type" content="website"/>
                <meta
                    property="og:title"
                    content="Bảo hiểm VietinBank (VBI) | Tiên phong công nghệ"
                />
                <meta
                    property="og:description"
                    content="Bảo hiểm VietinBank (VBI) là một trong những đơn vị tiên phong áp dụng công nghệ, cải tiến quy trình bồi thường với ứng dụng bảo hiểm số hàng đầu My VBI."
                />
                <meta
                    property="og:image"
                    content={process.env.NEXT_PUBLIC_SERVER+"images/img_thumbnail.jpg"}
                />
                <meta property="og:url" content="http://myvbi.vn/ "/>

                <link
                    href="https://api.mapbox.com/mapbox-gl-js/v1.10.1/mapbox-gl.css"
                    rel="stylesheet"
                />
            </Head>
            <Header_v2 isHome={isHome}/>
                {props.children}
        </div>
    );
};

export default MainLayout;
